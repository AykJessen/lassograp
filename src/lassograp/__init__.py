__version__ = "0.0.1"


from ._widget import example_magic_widget


#__all__ = (
#    "napari_get_reader",
#    "write_single_image",
#    "write_multiple",
#    "make_sample_data",
#    "example_magic_widget",
#)

__all__ = (
    "napari_get_reader",
    "write_single_image",
    "write_multiple",
    "make_sample_data",
    "example_magic_widget",
)
