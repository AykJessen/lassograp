"""
This module is an example of a barebones QWidget plugin for napari

It implements the Widget specification.
see: https://napari.org/stable/plugins/guides.html?#widgets

Replace code below according to your needs.
"""

from typing import List
from magicgui import magic_factory
import warnings
import numpy as np
import napari 
import cv2
from napari.types import LayerDataTuple
from qtpy.QtWidgets import QHBoxLayout, QPushButton, QWidget
from napari_plugin_engine import napari_hook_implementation


@napari_hook_implementation
def napari_experimental_provide_function():
    return [example_magic_widget]



def img_process(img_raw, blur_val):

    #First blur the image
    blur = cv2.GaussianBlur(img_raw,(5,5),0)

    #Than use a threshold (Otzu threshhold)
    ret3, th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU) 

    return th3

@magic_factory
def example_magic_widget(
                         blur_val: int,
                         layer: "napari.layers.Image",
                         layer_shapes: "napari.layers.Shapes",
                        ) -> List[LayerDataTuple]:

    #Init
    layer_data, layer_props, layer_type = layer.as_layer_data_tuple()
    img = layer.data
    shapes_layer = layer_shapes.data

    #Loop through shapes in shape layer
    for i, shape in enumerate(shapes_layer):
        coordinates_corners = [shape[0,0].astype(int),
                               shape[2,0].astype(int),
                               shape[0,1].astype(int),
                               shape[1,1].astype(int)]
        img_part = img[coordinates_corners[0]:coordinates_corners[1], coordinates_corners[2]:coordinates_corners[3]]
        img_part_return = img_process(img_part, blur_val)
        rows, cols = np.where(img_part_return > 0)
        img_part_return[rows, cols] = img_part[rows, cols]
        img[coordinates_corners[0]:coordinates_corners[1], coordinates_corners[2]:coordinates_corners[3]] = img_part_return

    grap_list = []  
    layer_props["name"] = layer_props["name"] + '_lassograp'
    grap_list.append((layer_data, layer_props, layer_type))
    return grap_list



